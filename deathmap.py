############
# author: gitlab.com/kieranpr
# created date: 29/03/20
# objective: coronavirus- interactive visual showing where the most deaths have happened
############

#notes
#reverted back to ipynb because it is better for displaying the folium map plot. there is not simple workaround in ipython (it doesn't like the html)
#reading from the settings.py file and the load_deaths_data.py could potentially be converted to ipynb too but needs tested

#bugs
#fix about 10 join countries/find somewhere to read world_coordinates online

########import packages
import pandas as pd
import folium
import settings as s

########get data
#death data from JSU github
deaths_df = pd.read_csv(s.url_deaths)

#get world co-ordinates from local csv as not possible to group by using JHU dataset
geo_df = pd.read_csv(s.coordinates_file_path)

#NEW
# URL for source data on cumulative covid19 deaths per country per day
#url_deaths = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
#deaths_df = pd.read_csv(url_deaths)

# path to local csv with world co-ordinates data
#coordinates_file_path = "/home/bitnami/coronadeaths.org/pyscripts/world_coordinates.csv"
#geo_df = pd.read_csv(coordinates_file_path)                                                                                          

########wrangle data
deaths_df = deaths_df.melt(id_vars=['Province/State', 'Country/Region', 'Lat', 'Long'])
deaths_df.rename(columns={
    'variable':'DATE',
    'value':'DEATHS',
    'Lat':'LATITUDE',
    'Long':'LONGITUDE',
    'Country/Region':'COUNTRY_REGION',
    'Province/State':'PROVINCE_STATE'
}, inplace=True)
deaths_df['DATE'] = pd.to_datetime(deaths_df['DATE'])

deaths_df.fillna(value='NA',inplace=True)

#select most recent day from dataset
maxdate = deaths_df['DATE'].max()

#filter to only get most recent death total
now_deaths_df = deaths_df[deaths_df['DATE']==maxdate]

#group by country
now_deaths_df = now_deaths_df.groupby('COUNTRY_REGION', as_index=False)\
    .agg({'DEATHS':'sum'})\
    .sort_values(by=['DEATHS'], ascending=False)
now_deaths_df

#remove country code column
geo_df = geo_df.drop(['Code'], axis=1)

#left join on country
# Q: (HRB) Does this join always work? What happens to countries which don't have a lat/long in geo_df? They get dropped in line 59?
# A: (KR) No, from memory, ~10 countries dont join because of various string differences. I didn't fix it as they were countries with few deaths. Could change; so needs fixed
deathmap_df = pd.merge(now_deaths_df, geo_df, left_on='COUNTRY_REGION', right_on='Country', how='left')
deathmap_df = deathmap_df.drop(['COUNTRY_REGION'], axis=1)

#drop countries with 0 deaths
deathmap_df = deathmap_df[deathmap_df.DEATHS != 0]

#drop NAs for speed
################### LAZY LAZY NEEDS CORRECTED
deathmap_df = deathmap_df.dropna()

######### create map

###########data viz
#begin map
m = folium.Map(location=[35,-3],
               zoom_start=2,
               tiles='cartodbdark_matter')

# design for data plot
for i in range(0,len(deathmap_df)):
   folium.Circle(
      location=[deathmap_df.iloc[i]['latitude'], deathmap_df.iloc[i]['longitude']],
      popup= ( '<strong>Country</strong>: ' '<br>' + str(deathmap_df.iloc[i]['Country']) + '<br>' '<strong>Deaths</strong>: ' + str(deathmap_df.iloc[i]['DEATHS']) + '<br>' '<strong>Updated:</strong>' '<br>' + str(maxdate.date())),
       radius=int(deathmap_df.iloc[i]['DEATHS'])*10,
      color='white', #crimson
      weight=0.5,
      fill=True,
      fill_color='white',
      fill_opacity=0.3
   ).add_to(m)

#save to html
m.save('map.html')
