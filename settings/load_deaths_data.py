import pandas as pd
import sys

import settings as s


def get_csv(url_deaths=s.url_deaths):
    """
    This function downloads a dataset of cumulative covid19 deaths per state/country per day
    from a source on the internet.

    We expect the url to be the path to a csv file with columns:
    'Province/State', 'Country/Region', 'Lat', 'Long', date1, date2, date3, ...
    where the dates are given in N American mm/dd/yy format.
    The values under each date should be the cumulative total covid19 deaths per region/country for that date.

    We unpivot the data before returning, so that date and cumulative number of deaths become columns.

    Parameters
    ----------
    url_deaths: str
        The path to the csv file to be downloaded - default value comes from the settings file

    Returns
    -------
    pd.DataFrame: with headers 'PROVINCE_STATE', 'COUNTRY_REGION', 'LATITUDE', 'LONGITUDE', 'DATE', 'DEATHS'
    """

    # Try to read the csv from the web - if this does not succeed (for whatever reason) print an error and exit the code
    try:
        deaths_df = pd.read_csv(url_deaths)
    except:
        print("Bad url for covid19 deaths dataset! File could not be downloaded. Exiting.")
        sys.exit(1)

    # Check that the dataset has the expected geographical columns - if not print an error and exit the code
    expected_geo_columns = ['Province/State', 'Country/Region', 'Lat', 'Long']
    try:
        assert all(column in deaths_df.columns for column in expected_geo_columns)
    except AssertionError:
        print("Covid19 deaths dataset doean't have the expected geographical columns! Exiting")
        sys.exit(1)

    # Unpivot the date columns to create a pair of new columns: DATE and DEATHS
    # Remove any punctuation from the other column headers
    deaths_df = deaths_df.melt(id_vars=expected_geo_columns)
    deaths_df.rename(columns={
        'Province/State': 'PROVINCE_STATE',
        'Country/Region': 'COUNTRY_REGION',
        'Lat': 'LATITUDE',
        'Long': 'LONGITUDE',
        'variable': 'DATE',
        'value': 'DEATHS'
    }, inplace=True)

    return deaths_df


def get_latest_deaths_per_country(deaths_df):
    """
    This function takes a table of cumulative covid19 deaths per state/country per day
    and extracts the latest total per country.

    We also do some data cleaning so that null values are replaced with 'NA'

    Parameters
    ----------
    deaths_df:  pd.DataFrame
        with headers 'PROVINCE_STATE', 'COUNTRY_REGION', 'LATITUDE', 'LONGITUDE', 'DATE', 'DEATHS'

    Returns
    -------
    pd.DataFrame: with columns 'DATE', 'COUNTRY_REGION', 'DEATHS'
    """

    # Convert the dates to pandas datetime objects, ensuring that the date format is interpreted correctly
    deaths_df['DATE'] = pd.to_datetime(deaths_df['DATE'], format='%m/%d/%y')

    # Replace any missing values with 'NA'
    # NOTE: this is not recommended!! Unless the table is intended for someone to read there's no need to do this
    deaths_df.fillna(value='NA', inplace=True)

    # select most recent day from dataset
    latest_date = deaths_df['DATE'].max()

    # filter to only get most recent death total
    latest_deaths_df = deaths_df[deaths_df['DATE'] == latest_date]

    # group by country and sum the total number of deaths across states/provinces, as necessary
    latest_deaths_df = latest_deaths_df.groupby(['DATE', 'COUNTRY_REGION'], as_index=False) \
        .agg({'DEATHS': 'sum'}) \
        .sort_values(by=['DEATHS'], ascending=False)

    return latest_deaths_df
