
# URL for source data on cumulative covid19 deaths per country per day
url_deaths = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"

# path to local csv with world co-ordinates data
coordinates_file_path = "world_coordinates.csv"
