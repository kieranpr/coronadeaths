import pandas as pd
import numpy as np
import plotly
import plotly.graph_objs as go

########get data from JHU github
url_deaths = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
deaths_df = pd.read_csv(url_deaths)

#######data wrangling
cols = deaths_df.keys()
# remove first 4 cols
deaths = deaths_df.loc[:, cols[4]:cols[-1]]

dates = deaths.keys()

total_deaths = []
australia_deaths = []
brazil_deaths = []
belgium_deaths = []
china_deaths = []
france_deaths = []
germany_deaths = []
italy_deaths = []
iran_deaths = []
japan_deaths = []
russia_deaths = []
spain_deaths = []
uk_deaths = []
us_deaths = []

for i in dates:
    death_sum = deaths[i].sum()

    total_deaths.append(death_sum)

    # by country
    australia_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Australia'][i].sum())
    brazil_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Brazil'][i].sum())
    belgium_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Belgium'][i].sum())
    china_deaths.append(deaths_df[deaths_df['Country/Region'] == 'China'][i].sum())
    france_deaths.append(deaths_df[deaths_df['Country/Region'] == 'France'][i].sum())
    germany_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Germany'][i].sum())
    italy_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Italy'][i].sum())
    iran_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Iran'][i].sum())
    japan_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Japan'][i].sum())
    russia_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Russia'][i].sum())
    spain_deaths.append(deaths_df[deaths_df['Country/Region'] == 'Spain'][i].sum())
    uk_deaths.append(deaths_df[deaths_df['Country/Region'] == 'United Kingdom'][i].sum())
    us_deaths.append(deaths_df[deaths_df['Country/Region'] == 'US'][i].sum())



#calculate 60 days of data for each country with lockdown date commented
aztest = australia_deaths[61:] #23Mar
brtest = brazil_deaths[103:] #5May
btest = belgium_deaths[57:] #17Mar
ctest = china_deaths[0:] #23Jan 17d
ftest = france_deaths[54:] #16Mar 149d
gtest = germany_deaths[58:] #20Mar
itest = italy_deaths[47:] #9Mar 463d
irtest = iran_deaths[74:] #5Apr
jtest = japan_deaths[76:] #7Apr
rtest = russia_deaths[68:] #30Mar
stest = spain_deaths[53:] #15Mar 289d
uktest = uk_deaths[61:] #23Mar 335d
ustest = us_deaths[58:] #20Mar


###########calculate days since lockdown
#not using future_forecast

australia_days_in_future = 0 #-64
australia_future_forecast = np.array([i for i in range(len(dates)+australia_days_in_future)]).reshape(-1, 1)
australia_lockdown_dates = australia_future_forecast[:]

brazil_days_in_future = 0 #-103
brazil_future_forecast = np.array([i for i in range(len(dates)+brazil_days_in_future)]).reshape(-1, 1)
brazil_lockdown_dates = brazil_future_forecast[:]

belgium_days_in_future = 0 #-65
belgium_future_forecast = np.array([i for i in range(len(dates)+belgium_days_in_future)]).reshape(-1, 1)
belgium_lockdown_dates = belgium_future_forecast[:]

china_days_in_future = 0 #-65
china_future_forecast = np.array([i for i in range(len(dates)+china_days_in_future)]).reshape(-1, 1)
china_lockdown_dates = china_future_forecast[:]

france_days_in_future = 0 #-65
france_future_forecast = np.array([i for i in range(len(dates)+france_days_in_future)]).reshape(-1, 1)
france_lockdown_dates = france_future_forecast[:]

germany_days_in_future = 0 #-65
germany_future_forecast = np.array([i for i in range(len(dates)+germany_days_in_future)]).reshape(-1, 1)
germany_lockdown_dates = germany_future_forecast[:]

italy_days_in_future = 0 #-65
italy_future_forecast = np.array([i for i in range(len(dates)+italy_days_in_future)]).reshape(-1, 1)
italy_lockdown_dates = italy_future_forecast[:]

iran_days_in_future = 0 #-74
iran_future_forecast = np.array([i for i in range(len(dates)+iran_days_in_future)]).reshape(-1, 1)
iran_lockdown_dates = iran_future_forecast[:]

japan_days_in_future = 0 #-76
japan_future_forecast = np.array([i for i in range(len(dates)+japan_days_in_future)]).reshape(-1, 1)
japan_lockdown_dates = japan_future_forecast[:]

russia_days_in_future = 0 #-65
russia_future_forecast = np.array([i for i in range(len(dates)+russia_days_in_future)]).reshape(-1, 1)
russia_lockdown_dates = russia_future_forecast[:]

spain_days_in_future = 0 #-65
spain_future_forecast = np.array([i for i in range(len(dates)+spain_days_in_future)]).reshape(-1, 1)
spain_lockdown_dates = spain_future_forecast[:]

uk_days_in_future = 0 #-65
uk_future_forecast = np.array([i for i in range(len(dates)+uk_days_in_future)]).reshape(-1, 1)
uk_lockdown_dates = uk_future_forecast[:]

us_days_in_future = 0 #-65
us_future_forecast = np.array([i for i in range(len(dates)+us_days_in_future)]).reshape(-1, 1)
us_lockdown_dates = us_future_forecast[:]


#########data viz 2 - interactive plot
#extra datawrangling necessary
#convert 2d numpy arryay objects to 1d list for plotly
brazil_lockdown_dates_1d = brazil_lockdown_dates.flatten().tolist()
china_lockdown_dates_1d = china_lockdown_dates.flatten().tolist()
germany_lockdown_dates_1d = germany_lockdown_dates.flatten().tolist()
italy_lockdown_dates_1d = italy_lockdown_dates.flatten().tolist()
japan_lockdown_dates_1d = japan_lockdown_dates.flatten().tolist()
russia_lockdown_dates_1d = russia_lockdown_dates.flatten().tolist()
uk_lockdown_dates_1d = uk_lockdown_dates.flatten().tolist()
us_lockdown_dates_1d = us_lockdown_dates.flatten().tolist()

# this is the plot

fig = go.Figure(
    # can this become a function rather than 10 plots? https://www.kaggle.com/alro10/coronavirus-brazil-plotly
    # couldnt add nice formatted labels, e.g. px.line(x=brtest2, y=brtest, labels={'x':'Day since lockdown', 'y':'Deaths'})
    [
        go.Scatter(x=china_lockdown_dates_1d, y=ctest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="China", line_color="#df2407"),
        go.Scatter(x=germany_lockdown_dates_1d, y=gtest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="Germany", line_color="#FFCC00"),
        go.Scatter(x=italy_lockdown_dates_1d, y=itest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="Italy", line_color="#008c45"),
        go.Scatter(x=japan_lockdown_dates_1d, y=jtest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="Japan", line_color="#ffffff"),
        go.Scatter(x=russia_lockdown_dates_1d, y=rtest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="Russia", line_color="#0598ed"),
        go.Scatter(x=uk_lockdown_dates_1d, y=uktest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="UK", line_color="#012169"),
        go.Scatter(x=us_lockdown_dates_1d, y=ustest, mode='markers+lines', text='Day of lockdown, Number of deaths',
                   name="US", line_color="#EE82EE")
    ]

)

fig.update_layout(
    title='<span style="font-size: 30px;"><b>Does an early lockdown save lives?</b></span> <br><span style="font-size: 19px;">Cumulative number of coronavirus related deaths since a major lockdown affecting millions</span>',
    xaxis_title="Number of days since a major lockdown affecting millions ➡️➡️➡️",
    xaxis=dict(title_font={"size": 19}),
    legend=dict(font=dict(size=19)),
    showlegend=True, template="plotly_dark",
    yaxis={'showgrid': False},
    yaxis_type="log")

fig.write_html("plotlylockdown.html")
fig.show()
