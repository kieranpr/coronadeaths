############
# author: gitlab.com/kieranpr
# created date: 29/03/20
# objective: coronavirus- webscraper to see whether there are confirmed deaths in every country
############

########import packages
from requests import get
from bs4 import BeautifulSoup
import pandas as pd
import datetime #for todays date

######get data
#webscrape countries with deaths
url = 'https://www.worldometers.info/coronavirus/'
response = get(url)
html_soup = BeautifulSoup(response.text, 'html.parser')

######read table
cases = html_soup.find_all('tr')
#print(cases)

#get other data - list of countries
url_countries = "https://raw.githubusercontent.com/stefangabos/world_countries/master/data/en/countries.csv"
#get data - read csv
countries = pd.read_csv(url_countries)

#########wrangle data
value = []
for i in range(0, len(cases)):
    l = cases[i].get_text().split('\n')[1:13]
    if l[1] == 'Country,Other':
        value.append(i)

col = ['Country', 'TotalCases', 'NewCases', 'TotalDeaths', 'NewDeaths', 'TotalRecovered', 'ActiveCases',
       'Serious_Critical', 'TotCases1Mpop', 'TotDeaths1Mpop']
df = pd.DataFrame()

Country = []
TotalCases = []
NewCases = []
TotalDeaths = []
NewDeaths = []
TotalRecovered = []
ActiveCases = []
Serious_Critical = []
TotCases1Mpop = []
TotDeaths1Mpop = []

for i in range(1, value[1]):
    l = cases[i].get_text().split('\n')[1:13]
    Country.append(l[1])
    TotalCases.append(l[2].replace(',', ''))
    NewCases.append(l[3].replace(',', ''))
    TotalDeaths.append(l[4].replace(',', ''))
    NewDeaths.append(l[5].replace(',', ''))  # 6 is a New recovered column
    TotalRecovered.append(l[7].replace(',', ''))
    ActiveCases.append(l[8].replace(',', ''))
    Serious_Critical.append(l[9].replace(',', ''))
    TotCases1Mpop.append(l[10].replace(',', ''))
    TotDeaths1Mpop.append(l[11].replace(',', ''))

df = pd.DataFrame(zip(Country, TotalCases, NewCases, TotalDeaths, NewDeaths, TotalRecovered,
                      ActiveCases, Serious_Critical, TotCases1Mpop, TotDeaths1Mpop), columns=col)

dfc = df[8:]  # remove continent totals
dfc = dfc.set_index('Country')
dfc = dfc.drop(['Diamond Princess', 'MS Zaandam', 'Total:'], axis=0)  # remove non countries/territories
# dfc.to_csv('worldometers.csv',header=True,index=False)

# find countries that have more than 1 death
dfc = dfc[dfc['TotalDeaths'] != ' ']
# print(dfc)

#convert to string
countries = countries['name'].astype(str)
worldocountries = dfc.index.astype(str)

#convert to dataframe
countries = pd.DataFrame({'country':countries})
worldocountries = pd.DataFrame({'country':worldocountries})

#outer join to countries
outer_join = pd.merge(worldocountries, countries, left_on='country', right_on='country', how='right', indicator = True)

#anti join to get the remaining countries that haven't been joined to the worldocountries list
anti = outer_join[-(outer_join._merge == 'both')].drop('_merge',axis=1)

#get rid of weird old df index
anti = anti.set_index('country')
anti = anti.reset_index()

#remove the countries that didnt join because of different spellings
anti = anti[~anti['country'].isin(['Bolivia (Plurinational State of)',
                               'Brunei Darussalam',
                               'Central African Republic',
                               'Congo, Democratic Republic of the',
                               "Côte d'Ivoire",
                               'Iran (Islamic Republic of)',
                               'Kiribati',
                               'Korea, Republic of',
                               "Lao People's Democratic Republic",
                               'Moldova, Republic of',
                               'Russian Federation',
                               'Syrian Arab Republic',
                               'Tanzania, United Republic of',
                               'Viet Nam',
                               'Venezuela (Bolivarian Republic of)',
                               'United Kingdom of Great Britain and Northern Ireland',
                               'United States of America',
                               'United Arab Emirates'])]

#tidy up index and dataframe
anti = anti.set_index('country')
anti = anti.reset_index()
#anti = anti.set_index('country')
#print(anti)

#total countries + territories with cases
dfcount = (len(anti.index))

#get todays date
now = datetime.datetime.now()


def everywhere():
    if dfcount >= 195:
        return "YES, corona is everywhere. :(</p>"
    else:
        return "No. Coronavirus isn't everywhere. As of " + str(
            now.day) + "/" + str(now.month) + "/" + str(now.year) + ", " + str(
            dfcount) + " countries have had no coronavirus related deaths. They are: </p>"


everywherehtml = everywhere()

tophtml = """
<DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">  <!-- Google web font "Open Sans" -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">

  <link rel="stylesheet" href="css/demo.css" />
  <link rel="stylesheet" href="css/templatemo-style.css">

  <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>

    </head>

        <body>

            <div id="particles-js"></div>
                <div style='padding: 50px;'>
		<header class="mb-5"><h1>coronadeaths</h1></header>
                <P class='mb-5' style='font-size:100%;'>According to the UN, there are 195 countries. This project uses webscraping🔮✨ to tell us that... 
"""

bottomhtml = """
		</div>
             </div>
        </body>

 <script type="text/javascript" src="js/particles.js"></script>
 <script type="text/javascript" src="js/app.js"></script>
</html>
"""

nodeathstablehtml = anti.to_html(index=False, justify='left', border=0, header = False)

with open("deathseverywhere.html", "w") as file:
    file.write(tophtml + everywherehtml + nodeathstablehtml + bottomhtml)
